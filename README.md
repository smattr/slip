# Slip

Once upon a time a naïve young hacker needed to do a presentation and didn't
have any fancy tools like Keynote or Powerpoint on hand. He wanted to use
pictures and animation and LaTeX/Beamer just wasn't cutting it. He could get
everything he needed done as SVG images and reasoned that there was no reason
not to use a web browser. After all, it can display SVG images, videos, flash
and all manner of other things. Hell, even HTML.

To use this tool:

1. Create each slide as an image in the subdirectory imgs/;
2. Run `make` to generate slides.js;
3. Open index.html in a browser;
4. Use the arrow keys to navigate forwards or backwards.

Remember to make sure the filenames of your slides are alphabetically in the
order you would like them to appear.

Disclaimers:

* I am not a web developer. It is likely my inept use of HTML will make you
  cringe.
* Only tested in Chromium. I make no guarantees this will work in other
  browsers.
* Licensed under the [WTFPL](http://www.wtfpl.net). Go crazy, kids.
